import os
import openai
import pandas as pd 
from SPARQLWrapper import SPARQLWrapper, JSON
import rdflib
from rdflib import Graph, ConjunctiveGraph, URIRef
from rdflib.namespace import RDF, RDFS
import streamlit as st

openai.api_key = os.getenv('OPENAI_API_KEY')
openai.api_type = os.getenv('OPENAI_API_TYPE')
openai.api_version = os.getenv('OPENAI_API_VERSION')
openai.api_base = os.getenv('OPENAI_API_BASE')
openai_engine = os.getenv('OPENAI_ENGINE')

st.cache_data(ttl=None)
def create_reduced_ontology():
    sparql_query_classes = "select distinct ?class FROM <http://data.europa.eu/949/graph/rinf> where { ?subj a ?class . }"
    sparql_query_properties = "select distinct ?pred FROM <http://data.europa.eu/949/graph/rinf> where { ?subj ?pred ?obj . }"
    
    res1 = execute_sparql_query(os.getenv('SPARQL_ENDPOINT'), sparql_query_classes)
    res2 = execute_sparql_query(os.getenv('SPARQL_ENDPOINT'), sparql_query_properties)
    
    relevant_classes = []
    for item in res1["results"]["bindings"]:
        v = item.get("class", {}).get("value", "")
        relevant_classes.append(v)
    
    relevant_properties = []
    for item in res2["results"]["bindings"]:
        v = item.get("pred", {}).get("value", "")
        relevant_properties.append(v)

    # Load the original ontology
    g = Graph()
    g.parse(os.getenv('ONTOLOGY_TTL'))

    # Create a new graph for the reduced ontology
    g_reduced = Graph()

    # Copy over the relevant classes and properties
    for cls in relevant_classes:
        g_reduced += g.triples((URIRef(cls), None, None))
    for prop in relevant_properties:
        g_reduced += g.triples((None, prop, None))

    # Copy over any additional triples that involve the relevant items
    for s, p, o in g:
        if s in relevant_classes or p in relevant_properties or o in relevant_classes:
            g_reduced.add((s, p, o))

    # Save the reduced ontology
    #print("Save the reduced ontology")
    #g_reduced.serialize('reduced_ontology.owl', format='turtle')
    return g_reduced

st.cache_data(ttl=None)
def load_ontologies():
    cg = ConjunctiveGraph()
    
    cg.parse("reduced_ontology.owl")

    return cg

st.cache_data(ttl=None)
def extract_ontology_information(graph):
    classes = set()
    properties = set()
    instances = set()

    for s, p, o in graph:
        if p == rdflib.RDF.type and o != rdflib.RDFS.Class:
            classes.add(str(o))
            instances.add(str(s))
        else:
            properties.add(str(p))

    return {
        "classes": list(classes),
        "properties": list(properties),
        "instances": list(instances),
    }

def generate_sparql_query(user_text, ontology_information):
    ontology_info_text = "\n".join(
        f"{key}: {', '.join(values)}"
        for key, values in ontology_information.items()
    )

    data_properties_text = ", ".join(ontology_information["properties"])

    prompt = (
        f"Ontology information:\n{ontology_info_text}\n\n"
        f"Data properties: {data_properties_text}\n\n"
        f"Generate a SPARQL query, not using any prefixes, based on the following text. Only return the query with no markdown formatting: '{user_text}'"
    )

    response = openai.ChatCompletion.create(
        engine=openai_engine,
        messages=[{"role": "user", "content": prompt}],
        max_tokens=200,
        n=1,
        stop=None,
        temperature=0.5,
        top_p=1,
        frequency_penalty=0,
        presence_penalty=0
    )

    generated_query = response.choices[0].message.content.strip()

    return generated_query

def execute_sparql_query(endpoint_url, query):
    sparql = SPARQLWrapper(endpoint_url)
    sparql.setQuery(query)
    sparql.setReturnFormat(JSON)
    result = sparql.query().convert()

    return result

def json_result_to_dataframe(json_result):
    cols = json_result["head"]["vars"]
    data = [
        {key: value["value"] for key, value in row.items()}
        for row in json_result["results"]["bindings"]
    ]
    df = pd.DataFrame(data, columns=cols)
    return df